const stripe = require("stripe")("sk_test_yqY1HGxVIfTXjyznM1mvt6Cc00rN6niIhc");

// this request comes from the web app, goes through nodejs express app and lands here
// check user is logged in, user is requesting the portal for himself and not another user
async function getCustomerPortalLink(req, res) {
  const customer = req.body.customerId;
  const session = await stripe.billingPortal.sessions.create({
    customer,
    return_url: "https://example.com/account",
  });
  res.send({ url: session.url });
}

module.exports = CustomerPortal;