/*

RULES FOR FIREBASE DATABASE. THIS MEANS THE API KEY IS INCLUDED IN EVERY CALL. ALL CALLS GO THROUGH HTTPS

{
  "rules": {
    ".read": false,
    ".write": "auth.token.api_key == '<YOUR_API_KEY>'"
  }
}

ALL SENSITIVE DATA MUST(STRIPE API KEY, FIREBASE API KEY) BE STORED IN ENVIRONMENT VARIABLES WITH DOTENV

*/
const admin = require('firebase-admin');
const serviceAccount = require('./serviceAccountKey.json');

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://<DATABASE_NAME>.firebaseio.com"
});

const databaseRef = admin.database().ref('/stripe_customers');


// ???
async function updateStripeCustomer(userId, customerId) {
  try {
    const response = await axios.patch(`${FIREBASE_DB_URL}/${userId}.json`, {
      stripe_customer_id: customerId,
    }, {
      headers: {
        'Authorization': `Bearer ${API_KEY}`,
      },
    });
    console.log(response.data);
  } catch (error) {
    console.error(error);
    throw error;
  }
}

// need to include headers
/*
    headers: {
        'Authorization': `Bearer ${API_KEY}`,
      },
*/

async function startStripePlanAndAddToFirebase(userId) {
  try {
    // Create a Stripe customer
    const customer = await stripe.customers.create({
      email: 'user@example.com', // Replace with the user's email
    });

    // Subscribe the customer to the plan with no charge and 20 credits
    await stripe.subscriptions.create({
      customer: customer.id,
      items: [
        {
          plan: 'plan_your_stripe_plan_id', // Replace with the Stripe plan ID
        },
      ],
      trial_end: 'now',
    });

    // Add the Stripe customer ID and credits information to the Firebase database  
    const userRef = databaseRef.child(userId);
    await userRef.update({
      stripe_customer_id: customer.id,
      credits: 20,
    });
  } catch (error) {
    console.error(error);
    throw error;
  }
}

// write a function that autorizes the consumption of credits
async function consumeCredits(userId, creditsToConsume) {

}

// write a simple expressjs route that receives a request from a firebase user to consume credits
// only authroize the credit consumption if
// a. firebase user is logged in
// b. user logged in has same id as the user for which we are trying to consume credits for
// c. there are enough credits