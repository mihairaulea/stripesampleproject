const functions = require('firebase-functions');
const admin = require('firebase-admin');
const firebaseConfig = JSON.parse(process.env.FIREBASE_CONFIG);
const axios = require('axios');
const stripe = require('stripe')(functions.config().stripe.token);

describe('incrementCreditCount', () => {
  let userId;
  let stripeCustomerId;

  beforeEach(async () => {
    userId = Date.now().toString();
    stripeCustomerId = (await stripe.customers.create()).id;
    await admin.database().ref(`stripe_customers/${userId}`).set({ stripe_customer_id: stripeCustomerId });
  });

  afterEach(async () => {
    await stripe.customers.del(stripeCustomerId);
    await admin.database().ref(`stripe_customers/${userId}`).remove();
  });

  it('increments the credit count in the Stripe plan', async () => {
    const incrementCreditCount = require('../incrementCreditCount');
    await incrementCreditCount(userId, 10);

    const stripeCustomer = (await stripe.customers.retrieve(stripeCustomerId)).subscriptions.data[0];
    const firebaseCustomer = (await admin.database().ref(`stripe_customers/${userId}`).once('value')).val();

    expect(stripeCustomer.items.data[0].quantity).toEqual(10);
    expect(firebaseCustomer.credit_count).toEqual(10);
  });
  
});
