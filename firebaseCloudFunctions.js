const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();


//We need to handle the following triggers from stripe:

// customer.subscription.created
// invoice.payment_succeeded
// customer.subscription.updated
// customer.subscription.deleted
// invoice.payment_failed
// charge.succeeded --- for one-time payment for credit refill

exports.subscriptionCreated = functions.stripe.webhooks.onEvent(async (event, context) => {
  switch (event.type) {
    case 'customer.subscription.created':
      const stripeCustomerId = event.data.object.customer;
      const subscriptionId = event.data.object.id;
      const planId = event.data.object.plan.id;
      const userId = event.data.object.metadata.firebase_user_id;

      const firebaseCustomerRef = admin.database().ref(`stripe_customers/${userId}`);
      const firebaseCustomer = (await firebaseCustomerRef.once('value')).val();

      // If the customer already exists in Firebase, update the subscription information
      if (firebaseCustomer) {
        firebaseCustomerRef.update({
          stripe_customer_id: stripeCustomerId,
          subscription_id: subscriptionId,
          plan_id: planId,
        });
      } else {
        // If the customer does not exist in Firebase, create a new customer record
        firebaseCustomerRef.set({
          stripe_customer_id: stripeCustomerId,
          subscription_id: subscriptionId,
          plan_id: planId,
          credit_count: 0,
        });
      }
      break;
    default:
      return;
  }
});


exports.invoicePaymentSucceeded = functions.stripe.webhooks.onEvent(async (event, context) => {
  switch (event.type) {
    case 'invoice.payment_succeeded':
      const stripeCustomerId = event.data.object.customer;
      const subscriptionId = event.data.object.subscription;
      const planId = event.data.object.lines.data[0].plan.id;
      const userId = event.data.object.metadata.firebase_user_id;

      const firebaseCustomerRef = admin.database().ref(`stripe_customers/${userId}`);
      const firebaseCustomer = (await firebaseCustomerRef.once('value')).val();

      // Check if the customer exists in Firebase
      if (firebaseCustomer) {
        // Increment the credit count based on the plan
        let creditCount = firebaseCustomer.credit_count;
        switch (planId) {
          case 'plan_1':
            creditCount += 10;
            break;
          case 'plan_2':
            creditCount += 100;
            break;
          case 'plan_3':
            creditCount += 1000;
            break;
        }

        // Update the credit count in Firebase
        firebaseCustomerRef.update({
          credit_count: creditCount,
        });
      } else {
        // If the customer does not exist in Firebase, return an error
        throw new Error(`No Firebase customer found for user ID: ${userId}`);
      }
      break;
    default:
      return;
  }
});

exports.customerSubscriptionUpdated = functions.stripe.webhooks.onEvent(async (event, context) => {
  switch (event.type) {
    case 'customer.subscription.updated':
      const stripeCustomerId = event.data.object.customer;
      const subscriptionId = event.data.object.id;
      const planId = event.data.object.plan.id;
      const userId = event.data.object.metadata.firebase_user_id;

      const firebaseCustomerRef = admin.database().ref(`stripe_customers/${userId}`);
      const firebaseCustomer = (await firebaseCustomerRef.once('value')).val();

      // Check if the customer exists in Firebase
      if (firebaseCustomer) {
        // Increment the credit count based on the new plan
        let creditCount = firebaseCustomer.credit_count;
        switch (planId) {
          case 'plan_1':
            creditCount = 10;
            break;
          case 'plan_2':
            creditCount = 100;
            break;
          case 'plan_3':
            creditCount = 1000;
            break;
        }

        // Update the credit count in Firebase
        firebaseCustomerRef.update({
          credit_count: creditCount,
        });
      } else {
        // If the customer does not exist in Firebase, return an error
        throw new Error(`No Firebase customer found for user ID: ${userId}`);
      }
      break;
    default:
      return;
  }
});

exports.customerSubscriptionDeleted = functions.stripe.webhooks.onEvent(async (event, context) => {
  switch (event.type) {
    case 'customer.subscription.deleted':
      const stripeCustomerId = event.data.object.customer;
      const userId = event.data.object.metadata.firebase_user_id;

      const firebaseCustomerRef = admin.database().ref(`stripe_customers/${userId}`);
      const firebaseCustomer = (await firebaseCustomerRef.once('value')).val();

      // Check if the customer exists in Firebase
      if (firebaseCustomer) {
        // Update the credit count in Firebase to 0
        firebaseCustomerRef.update({
          credit_count: 0,
        });
      } else {
        // If the customer does not exist in Firebase, return an error
        throw new Error(`No Firebase customer found for user ID: ${userId}`);
      }
      break;
    default:
      return;
  }
});

exports.invoicePaymentFailed = functions.stripe.webhooks.onEvent(async (event, context) => {
    switch (event.type) {
      case 'invoice.payment_failed':
        const stripeCustomerId = event.data.object.customer;
        const userId = event.data.object.metadata.firebase_user_id;
  
        const firebaseCustomerRef = admin.database().ref(`stripe_customers/${userId}`);
        const firebaseCustomer = (await firebaseCustomerRef.once('value')).val();
  
        // Check if the customer exists in Firebase
        if (firebaseCustomer) {
          // Update the credit count in Firebase to 0
          firebaseCustomerRef.update({
            credit_count: 0,
          });
        } else {
          // If the customer does not exist in Firebase, return an error
          throw new Error(`No Firebase customer found for user ID: ${userId}`);
        }
        break;
      default:
        return;
    }
  });

  exports.oneTimePayment = functions.stripe.webhooks.onEvent(async (event, context) => {
    switch (event.type) {
      case 'charge.succeeded':
        const stripeCustomerId = event.data.object.customer;
        const userId = event.data.object.metadata.firebase_user_id;
  
        const firebaseCustomerRef = admin.database().ref(`stripe_customers/${userId}`);
        const firebaseCustomer = (await firebaseCustomerRef.once('value')).val();
  
        // Check if the customer exists in Firebase
        if (firebaseCustomer) {
          // Increment the credit count in Firebase by 1000
          firebaseCustomerRef.update({
            credit_count: firebaseCustomer.credit_count + 1000,
          });
        } else {
          // If the customer does not exist in Firebase, return an error
          throw new Error('No Firebase customer found for this id');
        }
    }
});